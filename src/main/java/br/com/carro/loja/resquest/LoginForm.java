package br.com.carro.loja.resquest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class LoginForm {
	
	private String email;
	private String senha;
	
	public String getNome() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public UsernamePasswordAuthenticationToken converte() {
		
		return new UsernamePasswordAuthenticationToken(email,senha);
	}

}
