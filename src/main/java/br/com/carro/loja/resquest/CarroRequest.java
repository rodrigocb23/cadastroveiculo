package br.com.carro.loja.resquest;

import br.com.carro.loja.entity.Carro;

public class CarroRequest {
	
	private String nome;
	private String tipo;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Carro converter() {
		return new Carro(nome, tipo);
	}
	

}
