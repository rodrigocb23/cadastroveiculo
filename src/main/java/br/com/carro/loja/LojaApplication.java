package br.com.carro.loja;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import br.com.carro.loja.controller.UploadController;

@SpringBootApplication
@EnableSpringDataWebSupport
@EnableCaching
@ComponentScan({ "br.com.carro.loja", "br.com.carro.loja.controller" })
public class LojaApplication {

	public static void main(String[] args) {

		new File(UploadController.uploadDiretorio).mkdir();

		SpringApplication.run(LojaApplication.class, args);
	}

}
