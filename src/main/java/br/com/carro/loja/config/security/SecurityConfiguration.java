package br.com.carro.loja.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.carro.loja.repository.UsuarioRepository;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private AutenticacaoService autenticacaoService;
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	 @Override
	 @Bean
	protected AuthenticationManager authenticationManager() throws Exception {
		 
		return super.authenticationManager();
	}
	
	//Configuração de autenticação (controle de acesso)
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(autenticacaoService).passwordEncoder(new BCryptPasswordEncoder());
		
	}
	//Configuração de autorização(quem pode acessar cada url)
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers(HttpMethod.GET,"/carro").permitAll()
		.antMatchers(HttpMethod.GET,"/usuario").permitAll()
		.antMatchers(HttpMethod.GET,"/carro/*").permitAll()
		.antMatchers(HttpMethod.GET,"/usuario/*").permitAll()
		.antMatchers(HttpMethod.POST,"/carro").permitAll()
		.antMatchers(HttpMethod.POST,"/usuario").permitAll()
		.antMatchers(HttpMethod.GET,"/").permitAll()
		.antMatchers(HttpMethod.POST,"/upload").permitAll()
		.antMatchers(HttpMethod.POST,"/uploadStatus").permitAll()
		.antMatchers(HttpMethod.PUT,"/carro/*").permitAll()
		.antMatchers(HttpMethod.DELETE,"/carro/*").permitAll()
		.antMatchers(HttpMethod.POST,"/auth").permitAll()
		.antMatchers(HttpMethod.GET,"/actuator/**").permitAll()
		.anyRequest().authenticated()
		.and().csrf().disable()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and().addFilterBefore(new AutenticacaoViaTokenFilter(tokenService, usuarioRepository), UsernamePasswordAuthenticationFilter.class);
		
	}
	//Configuração de recursos estaticos(requisições para css JavaScript imagens)
	@Override
	public void configure(WebSecurity web) throws Exception {
	
	}	

}
