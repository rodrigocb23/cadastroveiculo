package br.com.carro.loja.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/")
public class UploadController {
	
	public static String uploadDiretorio = "/home/default/Pictures";
	
	@GetMapping
	public String upLoadPage(Model model) {
		return "uploadView";
	}
	
	@PostMapping("/upload")
	public String upload(Model model, @RequestParam("file") MultipartFile[] files) throws IOException {
		
		StringBuilder nomeArquivo = new StringBuilder();
		
		for (MultipartFile file : files) {
			
			Path fileNameAndPath = Paths.get(uploadDiretorio, file.getOriginalFilename());
			nomeArquivo.append(file.getOriginalFilename()+" ");
			Files.write(fileNameAndPath, file.getBytes());
		}
		
		model.addAttribute("mensage", "	Arquivo salvo com sucesso " + nomeArquivo.toString());
		
		return "uploadStatus";
	}
	
	
	
	

//	@PostMapping("/upload")
//	public String singleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
//
//		if (file.isEmpty()) {
//			redirectAttributes.addFlashAttribute("message", "Por favor selecione um arquivo para enviar");
//			return "redirect:uploadStatus";
//		}
//		try {
//			byte[] bytes = file.getBytes();
//			Path path = Paths.get(uploadDiretorio + file.getOriginalFilename());
//			Files.write(path, bytes);
//
//			redirectAttributes.addFlashAttribute("message",
//					"Você enviou com sucesso '" + file.getOriginalFilename() + "'");
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return "redirect:/uploadStatus";
//
//	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		return "uploadStatus";
	}

}
