package br.com.carro.loja.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.carro.loja.entity.Carro;
import br.com.carro.loja.repository.CarroRepository;
import br.com.carro.loja.response.AtualizacaoCarroResponse;
import br.com.carro.loja.response.CarroResponse;
import br.com.carro.loja.response.DetalhesCarroResponse;
import br.com.carro.loja.resquest.CarroRequest;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/carro")
public class CarrosController {

	@Autowired
	private CarroRepository carroRepository;

	@GetMapping
	@Cacheable(value = "listaDeCarros")
	public Page<CarroResponse> lista(@RequestParam(required = false) String tipo,
			@PageableDefault(sort = "idCarro", direction = Direction.DESC, page = 1, size = 5) Pageable paginacao) {

		if (tipo == null) {
			Page<Carro> carros = carroRepository.findAll(paginacao);
			return CarroResponse.converter(carros);
		} else {
			Page<Carro> carros = carroRepository.findByTipo(tipo, paginacao);
			return CarroResponse.converter(carros);
		}
	}

	@PostMapping
	@Transactional
	@CacheEvict(value = "listaDeCarros", allEntries = true)
	public ResponseEntity<CarroResponse> cadastrar(@RequestBody CarroRequest model, UriComponentsBuilder uriBuilder) {
		Carro carro = model.converter();
		carroRepository.save(carro);

		URI uri = uriBuilder.path("/carro/{id}").buildAndExpand(carro.getIdCarro()).toUri();
		return ResponseEntity.created(uri).body(new CarroResponse(carro));

	}

	@GetMapping("/{id}")
	public ResponseEntity<DetalhesCarroResponse> detalhar(@PathVariable Long id) {

		Optional<Carro> carro = carroRepository.findById(id);
		if (carro.isPresent()) {
			return ResponseEntity.ok(new DetalhesCarroResponse(carro.get()));
		}

		return ResponseEntity.notFound().build();
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "listaDeCarros", allEntries = true)
	public ResponseEntity<CarroResponse> atualizar(@PathVariable Long id,
			@RequestBody @Valid AtualizacaoCarroResponse model) {
		Carro carro = model.atualizar(id, carroRepository);

		return ResponseEntity.ok(new CarroResponse(carro));

	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> remover(@PathVariable Long id) {
		carroRepository.deleteById(id);
		return ResponseEntity.ok().build();
	}

}
