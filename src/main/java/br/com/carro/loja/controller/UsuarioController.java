package br.com.carro.loja.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.carro.loja.entity.Usuario;
import br.com.carro.loja.repository.UsuarioRepository;

@RestController
@RequestMapping("/usuario")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@GetMapping
	public List<Usuario> listarUsuarios(){
		
		return usuarioRepository.findAll();
	}
	
	@PostMapping
	public ResponseEntity<Usuario> cadastrarUsuario(@RequestBody Usuario model){
		 
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(model.getSenha());
		
		model.setSenha(encodedPassword);
		Usuario usuario = usuarioRepository.save(model);
		
		return ResponseEntity.ok(usuario);
		
	}
	
	@GetMapping("/{id}")
	public Usuario getUsuario(@PathVariable Long id) {
		
		return usuarioRepository.getOne(id);
	}
	

}
