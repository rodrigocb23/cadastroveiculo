package br.com.carro.loja.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.carro.loja.entity.Carro;

public interface CarroRepository extends JpaRepository<Carro, Long>{

	Page<Carro> findByTipo(String tipo, Pageable paginacao);
	
	List<Carro> findByTipo(String tipo);
	

}
