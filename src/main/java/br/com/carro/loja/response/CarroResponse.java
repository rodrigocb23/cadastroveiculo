package br.com.carro.loja.response;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import br.com.carro.loja.entity.Carro;

public class CarroResponse {
	
	private Long idCarro;
	private String nome;
	private String tipo;
	
	public CarroResponse( Carro carro) {
		this.idCarro = carro.getIdCarro();
		this.nome = carro.getNome();
		this.tipo = carro.getTipo();
		
	}
	
	public Long getIdCarro() {
		return idCarro;
	}
	public String getNome() {
		return nome;
	}
	
	public String getTipo() {
		return tipo;
	}

	public static Page<CarroResponse> converter(Page<Carro> carros) {
		
		return carros.map(CarroResponse::new);
	//return carros.stream().map(CarroResponse::new).collect(Collectors.toList());
	}
	
	public static List<CarroResponse> convertList(List<Carro> carros) {
		return carros.stream().map(CarroResponse::new).collect(Collectors.toList());
	}
}
