package br.com.carro.loja.response;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.carro.loja.entity.Carro;
import br.com.carro.loja.repository.CarroRepository;

public class AtualizacaoCarroResponse {

	@NotNull @NotBlank
	private String nome;
	
	@NotNull @NotBlank
	private String tipo;
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Carro atualizar(Long id, CarroRepository carroRepository) {
		Carro carro = carroRepository.getOne(id);
		carro.setNome(this.nome);
		carro.setTipo(this.tipo);
		
		return carro;
	}
	
	
	
}
