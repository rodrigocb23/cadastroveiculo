package br.com.carro.loja.response;

import java.util.List;
import java.util.stream.Collectors;

import br.com.carro.loja.entity.Carro;

public class DetalhesCarroResponse {
	
	private Long idCarro;
	private String nome;
	private String tipo;
	
	public DetalhesCarroResponse( Carro carro) {
		this.idCarro = carro.getIdCarro();
		this.nome = carro.getNome();
		this.tipo = carro.getTipo();
		
	}
	
	public Long getIdCarro() {
		return idCarro;
	}
	public String getNome() {
		return nome;
	}
	
	public String getTipo() {
		return tipo;
	}

	public static List<DetalhesCarroResponse> converter(List<Carro> carros) {
		
		return carros.stream().map(DetalhesCarroResponse::new).collect(Collectors.toList());
	}

}
